FROM inovedados/php:8.2-apache-node18-puppeteer

# Enable soap for fiscal rps
USER root

ENV PUPPETEER_SKIP_DOWNLOAD=true

RUN echo "max_execution_time=300\npost_max_size=500M\nmax_file_uploads=500\nupload_max_filesize=500M\nmemory_limit=1024M" > /usr/local/etc/php/conf.d/custom_params.ini

RUN apt-get update && apt-get install -y \
    libsqlite3-dev \
    libonig-dev


# Install PHP extensions using docker-php-ext-install
RUN docker-php-ext-install \
    curl \
    xml \
    pdo_sqlite \
    bcmath \
    gd \
    intl \
    mbstring \
    pdo \
    zip \
    dom \
    soap

# Install PECL extensions and enable them
RUN pecl install imagick pcov \
    && docker-php-ext-enable imagick pcov

# Copy package.json and composer.json
COPY --chown=www-data:www-data . /var/www/html

# Change to default user
USER www-data
