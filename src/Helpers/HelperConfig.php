<?php

namespace Knuckles\Scribe\Helpers;

use Knuckles\Scribe\Tools\DocumentationConfig;

class HelperConfig
{
    public static function getAuthConfig(): array
    {
        $config = new DocumentationConfig(config('scribe', []));
        if (!$config->get('auth.enabled', false)) {
            return [];
        }

        $securitySchemes = [];

        $auths = $config->get('auth.auths', []);

        foreach ($auths as $key => $value)
        {
            $in = $value['in'] ?? null;
            $name = $value['name'] ?? null;
            $extra_info = $value['extra_info'] ?? '';

            if (!$in || !$name)
            {
                throw new \Exception("Auth configuration for '$key' is missing 'in' or 'name' fields.");
            }

            $scheme = match ($in)
            {
                'query', 'header' => [
                    'type' => 'apiKey',
                    'name' => $name,
                    'in' => $in,
                    'description' => $extra_info,
                ],
                'bearer', 'basic' => [
                    'type' => 'http',
                    'scheme' => $in,
                    'description' => $extra_info,
                ],
                default => [],
            };

            $securitySchemes[$key] = $scheme;
        }

        $in = $config->get('auth.in');
        $name = $config->get('auth.name');
        $description = $config->get('auth.extra_info');

        $securitySchemes['default'] = match ($in)
        {
            'query', 'header' => [
                'type' => 'apiKey',
                'name' => $name,
                'in' => $in,
                'description' => $description,
            ],
            'bearer', 'basic' => [
                'type' => 'http',
                'scheme' => $in,
                'description' => $description,
            ],
            default => [],
        };

        return $auths;
    }
}
